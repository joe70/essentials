/*
 * Main.cpp
 *
 *  Created on: 1 may. 2019
 *      Author: joe
 */
#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "Animator.h"
#include <iostream>

int main() {
	sf::RenderWindow window(sf::VideoMode(640, 480), "AssetManager", sf::Style::Default);
	AssetManager manager;

	auto wSize=window.getSize();

	sf::View gameView(sf::Vector2f(0,0), sf::Vector2f(wSize.x, wSize.y));
	gameView.setViewport(sf::FloatRect(0,0,1,1));

	sf::View miniMap(sf::Vector2f(0,0), sf::Vector2f(wSize.x, wSize.y));
	miniMap.setViewport(sf::FloatRect(0.75f, 0, 0.25f, 0.25f));
	miniMap.setRotation(0.f);
	miniMap.zoom(1.0f);

	sf::Text text("Esto es una prueba", AssetManager::GetFont("fonts/zombiecontrol.ttf"));
	text.setFillColor(sf::Color::White);
	text.setCharacterSize(35);


	sf::Vector2i spriteSize(80, 80);
	sf::Sprite sprite;
	Animator animator(sprite);

	// Idle animation with 8 frames @1 sec looping
	auto& idleAnimation = animator.CreateAnimation("Idle", "graphics/spriteSheet.png", sf::seconds(1.0f), true);
	idleAnimation.AddFrames(sf::Vector2i(0,0), spriteSize, 8);

	//IdleShort animation with 8 frames @ 0.5 sec looping
	auto& idleAnimationShort = animator.CreateAnimation("IdleShort", "graphics/spriteSheet.png",\
														 sf::seconds(0.5f), true);
	idleAnimationShort.AddFrames(sf::Vector2i(0, 0), spriteSize, 8);

	//IdleSmall animation with 8 frames @1.5sec looping y tamaño diferente al inicial. Multiples Frames.
	auto& idleSmall = animator.CreateAnimation("IdleSmall", "graphics/crystals3216all.png",\
												sf::seconds(1.5f), true);
	idleSmall.AddFrames(sf::Vector2i(0,0), sf::Vector2i(31,31), 4);
	idleSmall.AddFrames(sf::Vector2i(0,95), sf::Vector2i(31,31), 4);

	// IdleOnce animation with 8 frames @ 0.5 sec not looping
	auto& idleAnimationOnce = animator.CreateAnimation("IdleOnce", "graphics/crystals3216all.png",\
													sf::seconds(0.5f), false);
	idleAnimationOnce.AddFrames(sf::Vector2i(0, 126), sf::Vector2i(31,31), 8);


	sf::Clock clock;
	while (window.isOpen()) {
		// Returns the elapsed time and restarts the clock
		sf::Time deltaTime = clock.restart();

		sf:: Event ev;
		while (window.pollEvent(ev)) {
			if (ev.type == sf::Event::EventType::Closed) {
				window.close();
			}
			if (ev.type == sf::Event::KeyPressed) {
				if (ev.key.code == sf::Keyboard::Key::Num1)
					animator.SwitchAnimation("Idle");
				else if (ev.key.code == sf::Keyboard::Key::Num2)
					animator.SwitchAnimation("IdleShort");
				else if (ev.key.code == sf::Keyboard::Key::Num3)
					animator.SwitchAnimation("IdleSmall");
				else if (ev.key.code == sf::Keyboard::Key::Num4)
					animator.SwitchAnimation("IdleOnce");
			}
		}

		animator.Update(deltaTime);

		window.clear(sf::Color::Black);

		window.setView(gameView);

		window.draw(sprite);

		window.setView(miniMap);

		window.draw(text);

		window.display();
	}
	return 0;
}


